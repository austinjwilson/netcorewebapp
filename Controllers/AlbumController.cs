﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using MySql.Data.MySqlClient;
using NetCoreWebApp.Models;


namespace NetCoreWebApp.Controllers
{
    public class AlbumController : Controller
    {
        
       
        public IActionResult Index()
        {
            MusicStoreContext context = HttpContext.RequestServices.GetService(typeof(NetCoreWebApp.Models.MusicStoreContext)) as MusicStoreContext;

            return View(context.GetAllAlbums());
        }
        public IActionResult Details(int? id)
        {

            return View();
        }
       
        public IActionResult Create()
        {
            //MusicStoreContext context = HttpContext.RequestServices.GetService(typeof(NetCoreWebApp.Models.MusicStoreContext)) as MusicStoreContext;
            //try
            //{
                //if (ModelState.IsValid)
                //{
                    //_context.Add(album);
                    //await _context.SaveChangesAsync();
                    //return RedirectToAction(nameof(Index));
                //}
            //}
            //catch (DbUpdateException /* ex */)
            //{
                //Log the error (uncomment ex variable name and write a log.
                //ModelState.AddModelError("", "Unable to save changes. " +
                    //"Try again, and if the problem persists " +
                    //"see your system administrator.");
            //}
            {
                return View();
            }
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(IFormCollection collection, string DefaultConnection)
        { 
            //Added 2/26/20 
            MusicStoreContext context = HttpContext.RequestServices.GetService(typeof(NetCoreWebApp.Models.MusicStoreContext)) as MusicStoreContext;

            
           // myList = collection<Album>;

            //string connectionString = @"server=localhost;port=3306;database=musicstore_db;user=root;password=#1Stunnas";

            MySqlConnection conn = null;
            try
            {
                Album myAlbum = new Album();
                myAlbum.Name = collection["Name"].ToString();
                myAlbum.ArtistName = collection["Artist Name"].ToString();
                myAlbum.Genre = collection["Genre"].ToString();
                myAlbum.Price = Decimal.Parse(collection["Price"].ToString());

                conn = new MySqlConnection(DefaultConnection);
                conn.Open();
                MySqlCommand cmd = new MySqlCommand();
                cmd.Connection = conn;
               // cmd.CommandText = "INSERT INTO Album(Name, ArtistName, Price, Genre) VALUES("Bob", "Austin", "Price", "Genre");
                //cmd.CommandText = "INSERT INTO Album(Name, ArtistName, Price, Genre) VALUES(@Name, @ArtistName, @Price, @Genre)";
                cmd.Prepare();

                cmd.Parameters.AddWithValue("...", "...");
                cmd.ExecuteNonQuery();

                return RedirectToAction(nameof(Index));
            }
            catch (NullReferenceException ex)
            {

                Console.WriteLine(ex.Message);
                return View(nameof(Create));
            }
            finally
            {
                if (conn != null) conn.Close();
            }
        }
        public IActionResult Edit(int id)
        {
            return View();
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, IFormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }
        public IActionResult Delete(int id)
        {
            return View();
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Delete(int id, IFormCollection collection)
        {
            //MusicStoreContext context = HttpContext.RequestServices.GetService(typeof(NetCoreWebApp.Models.MusicStoreContext)) as MusicStoreContext;
            //var album = await context.FindAsync(id);
            //if (album == null)
            //{
            //return RedirectToAction(nameof(Index));
            //}

            //try
            //{
            //context.Album.Remove(album);
            //await context.SaveChangesAsync();
            //return RedirectToAction(nameof(Index));
            //}
            //catch (DbUpdateException /* ex */)
            //{
            //Log the error (uncomment ex variable name and write a log.)
            return RedirectToAction(nameof(Delete), new { id = id, saveChangesError = true });
        }
    } // TODO: Add delete logic here
        }
        /*[HttpPost]
        public IActionResult NewForm(Models.Album sm)
        {
            ViewBag.Id = sm.Id;
            ViewBag.Name = sm.Name;
            ViewBag.ArtistName = sm.ArtistName;
            ViewBag.Price = sm.Price;
            ViewBag.genre = sm.Genre;

            return View("Index");
        }*/
    
    
